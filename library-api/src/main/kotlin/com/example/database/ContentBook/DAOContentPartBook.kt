package com.example.database.ContentBook

import com.example.database.DAOTable
import com.example.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOContentPartBook: DAOTable<PartBook> {
    override fun resultRowToModel(row: ResultRow): PartBook {
        return PartBook(
            id = row[ContentParts.id],
            title = row[ContentParts.title],
            idBook = row[ContentParts.idBook],
            index = row[ContentParts.index],
            page = row[ContentParts.page],
            hide = row[ContentParts.hide]
        )
    }

    override suspend fun selectAll(): List<PartBook> {
        return pushQuery {
            ContentParts.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): PartBook? {
        return pushQuery {
            ContentParts.select(where).map(::resultRowToModel).singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<PartBook> {
        return pushQuery {
            ContentParts.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            ContentParts.deleteWhere(op=where) > 0
        }
    }

    override suspend fun insert(model: PartBook): PartBook? {
        return pushQuery {
            ContentParts.insert {
                it[ContentParts.title] = model.title
                it[ContentParts.idBook] = model.idBook
                it[ContentParts.index] = model.index
                it[ContentParts.page] = model.page
                it[ContentParts.hide] = model.hide
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: PartBook, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            ContentParts.update(where) {
                it[ContentParts.title] = model.title
                it[ContentParts.idBook] = model.idBook
                it[ContentParts.index] = model.index
                it[ContentParts.page] = model.page
                it[ContentParts.hide] = model.hide
            } > 0
        }
    }

}