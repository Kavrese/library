package com.example.database.ContentBook

import org.jetbrains.exposed.sql.Table

data class PartBook(
    val id: Int,
    val title: String,
    val index: Int,
    val idBook: Int,
    val hide: Boolean,
    val page: Int?
){
    fun toPartBookChapters(chapters: List<ChapterBook>): PartBookChapters{
        return PartBookChapters(id, title, index, idBook, page, hide, chapters)
    }
}

@kotlinx.serialization.Serializable
data class PartBookChapters(
    val id: Int,
    val title: String,
    val index: Int,
    val idBook: Int,
    var page: Int?,
    val hide: Boolean,
    val chapters: List<ChapterBook>
){
    fun toPartBook(): PartBook{
        return PartBook(id, title, index, idBook, hide, page)
    }
}

@kotlinx.serialization.Serializable
data class ChapterBook(
    val id: Int,
    val name: String,
    val page: Int,
    val idPart: Int,
    val idBook: Int,
)

@kotlinx.serialization.Serializable
data class FullContentBook(
    val idBook: Int,
    val parts: List<PartBookChapters>
)

object ContentChapters: Table("content_chapters"){
    val id = integer("id").autoIncrement()
    val name = varchar("name", 100)
    val page = integer("page")
    val idBook = integer("id_book")
    val idPart = integer("id_part")

    override val primaryKey = PrimaryKey(id)
}

object ContentParts: Table("content_parts"){
    val id = integer("id").autoIncrement()
    val title = varchar("title", 100)
    val index = integer("index")
    val idBook = integer("id_book")
    val page = integer("page").nullable()
    val hide = bool("hide")

    override val primaryKey = PrimaryKey(id)
}
