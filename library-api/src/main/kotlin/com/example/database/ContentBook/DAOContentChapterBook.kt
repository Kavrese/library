package com.example.database.ContentBook

import com.example.database.DAOTable
import com.example.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOContentChapterBook: DAOTable<ChapterBook> {
    override fun resultRowToModel(row: ResultRow): ChapterBook {
        return ChapterBook(
            id = row[ContentChapters.id],
            name = row[ContentChapters.name],
            page = row[ContentChapters.page],
            idBook = row[ContentChapters.idBook],
            idPart = row[ContentChapters.idPart]
        )
    }

    override suspend fun selectAll(): List<ChapterBook> {
        return pushQuery {
            ContentChapters.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): ChapterBook? {
        return pushQuery {
            ContentChapters.select(where).map(::resultRowToModel).singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<ChapterBook> {
        return pushQuery {
            ContentChapters.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            ContentChapters.deleteWhere(op=where) > 0
        }
    }

    override suspend fun insert(model: ChapterBook): ChapterBook? {
        return pushQuery {
            ContentChapters.insert {
                it[ContentChapters.name] = model.name
                it[ContentChapters.idBook] = model.idBook
                it[ContentChapters.idPart] = model.idPart
                it[ContentChapters.page] = model.page
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: ChapterBook, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            ContentChapters.update(where) {
                it[ContentChapters.name] = model.name
                it[ContentChapters.idBook] = model.idBook
                it[ContentChapters.idPart] = model.idPart
                it[ContentChapters.page] = model.page
            } > 0
        }
    }

}