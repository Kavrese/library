package com.example.database.BookFile

import com.example.database.DAOTable
import com.example.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOBookFile: DAOTable<BookFile> {
    override fun resultRowToModel(row: ResultRow): BookFile {
        return BookFile(
            id = row[BookFiles.id],
            filePath = row[BookFiles.filePath],
            idBook = row[BookFiles.idBook],
            filename = row[BookFiles.filename]
        )
    }

    override suspend fun selectAll(): List<BookFile> {
        return pushQuery{
            BookFiles.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): BookFile? {
        return pushQuery {
            BookFiles.select(where).map(::resultRowToModel).singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<BookFile> {
        return pushQuery {
            BookFiles.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            BookFiles.deleteWhere(op = where) > 0
        }
    }

    override suspend fun insert(model: BookFile): BookFile? {
        return pushQuery {
            BookFiles.insert {
                it[BookFiles.idBook] = model.idBook
                it[BookFiles.filePath] = model.filePath
                it[BookFiles.filename] = model.filename
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: BookFile, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            BookFiles.update(where) {
                it[BookFiles.idBook] = model.idBook
                it[BookFiles.filename] = model.filename
                it[BookFiles.filePath] = model.filePath
            } > 0
        }
    }

    suspend fun checkExistBookFile(idBookFile: Int): Boolean{
        return selectSingle {
            BookFiles.id eq idBookFile
        } != null
    }

    suspend fun checkExistBookFile(file_path: String): Boolean{
        return selectSingle {
            BookFiles.filePath eq file_path
        } != null
    }

    suspend fun checkExistBookFile(file_path: String, idBook: Int): Boolean{
        return selectSingle {
            (BookFiles.filePath eq file_path) and (BookFiles.idBook eq idBook)
        } != null
    }
}