package com.example.database.BookFile

import org.jetbrains.exposed.sql.Table

@kotlinx.serialization.Serializable
data class BookFile(
    val id: Int,
    val filePath: String,
    val idBook: Int,
    val filename: String
)

object BookFiles: Table("files"){
    val id = integer("id").autoIncrement()
    val filePath = varchar("file_path", 500)
    val idBook = integer("id_book")
    val filename = varchar("filename", 100)

    override val primaryKey = PrimaryKey(id)
}