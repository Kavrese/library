package com.example.database.Author

import com.example.database.Book.Books
import com.example.database.DAOTable
import com.example.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOAuthor: DAOTable<Author> {
    override fun resultRowToModel(row: ResultRow): Author {
        return Author(
            row[Authors.id],
            row[Authors.name]
        )
    }

    override suspend fun selectAll(): List<Author> {
        return pushQuery{
            Authors.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): Author? {
        return pushQuery {
            Authors.select(where).map(::resultRowToModel).singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<Author> {
        return pushQuery {
            Authors.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Authors.deleteWhere(op = where) > 0
        }
    }

    override suspend fun insert(model: Author): Author? {
        return pushQuery {
            Authors.insert {
                it[Authors.name] = model.name
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: Author, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Authors.update(where) {
                it[Authors.name] = model.name
            } > 0
        }
    }

    suspend fun checkExistAuthor(name: String): Boolean{
        return selectSingle {
            Authors.name eq name
        } != null
    }
}