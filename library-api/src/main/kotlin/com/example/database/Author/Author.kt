package com.example.database.Author

import org.jetbrains.exposed.sql.Table

@kotlinx.serialization.Serializable
class Author {
    val id: Int
    val name: String

    constructor(id: Int, name: String) {
        this.name = name
        this.id = id
    }
}

@kotlinx.serialization.Serializable
class AuthorBook {
    val id: Int
    val idAuthor: Int
    val idBook: Int

    constructor(id: Int, idAuthor: Int, idBook: Int) {
        this.idAuthor = idAuthor
        this.idBook = idBook
        this.id = id
    }
}

object Authors: Table(){
    val id = integer("id").autoIncrement()
    val name = varchar("name", 100)

    override val primaryKey = PrimaryKey(id)
}

object AuthorsBook: Table("authors_book"){
    val id = integer("id").autoIncrement()
    val idAuthor = integer("id_author")
    val idBook = integer("id_book")

    override val primaryKey = PrimaryKey(id)
}