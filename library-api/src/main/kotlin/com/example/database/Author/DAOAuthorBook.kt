package com.example.database.Author

import com.example.database.DAOTable
import com.example.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOAuthorBook: DAOTable<AuthorBook> {
    override fun resultRowToModel(row: ResultRow): AuthorBook {
        return AuthorBook(
            id = row[AuthorsBook.id],
            idBook = row[AuthorsBook.idBook],
            idAuthor = row[AuthorsBook.idAuthor],
        )
    }

    override suspend fun selectAll(): List<AuthorBook> {
        return AuthorsBook.selectAll().map(::resultRowToModel)
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): AuthorBook? {
        return pushQuery{
            AuthorsBook.select(where).map(::resultRowToModel).singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<AuthorBook> {
        return pushQuery{
            AuthorsBook.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            AuthorsBook.deleteWhere(op = where) > 0
        }
    }

    override suspend fun insert(model: AuthorBook): AuthorBook? {
        return pushQuery {
            AuthorsBook.insert {
                it[AuthorsBook.idAuthor] = model.idAuthor
                it[AuthorsBook.idBook] = model.idBook
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: AuthorBook, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            AuthorsBook.update(where) {
                it[AuthorsBook.idBook] = model.idBook
                it[AuthorsBook.idAuthor] = model.idAuthor
            } > 0
        }
    }

}