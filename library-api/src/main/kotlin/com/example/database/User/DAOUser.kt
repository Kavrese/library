package com.example.database.User

import com.example.database.DAOTable
import com.example.database.DatabaseFactory
import org.jetbrains.exposed.sql.*

class DAOUser: DAOTable<User> {

    override suspend fun edit(model: User, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return DatabaseFactory.pushQuery {
            Users.update(where) {
                it[firstname] = model.firstname
                it[avatar] = model.avatar
                it[password] = model.password
                it[`class`] = model.`class`
            } > 0
        }
    }

    override fun resultRowToModel(row: ResultRow): User = User(
        id = row[Users.id],
        firstname = row[Users.firstname],
        `class` = row[Users.`class`],
        avatar = row[Users.avatar],
        password = row[Users.password]
    )

    override suspend fun selectAll(): List<User> {
        return DatabaseFactory.pushQuery {
            Users.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): User? {
        return DatabaseFactory.pushQuery {
            Users.select(where)
                .map(::resultRowToModel)
                .singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<User> {
        return DatabaseFactory.pushQuery {
            Users.select(where)
                .map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return DatabaseFactory.pushQuery {
            Users.deleteWhere(op = where) > 0
        }
    }

    override suspend fun insert(model: User): User? {
        return DatabaseFactory.pushQuery {
            Users.insert {
                it[firstname] = model.firstname
                it[password] = model.password
                it[avatar] = model.avatar
                it[`class`] = model.`class`
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    suspend fun checkExistUserName(name: String): Boolean{
        return selectSingle { Users.firstname eq name } != null
    }
}