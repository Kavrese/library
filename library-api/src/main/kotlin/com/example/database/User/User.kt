package com.example.database.User

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

class User(
    val id: Int,
    override val firstname: String,
    override val password: String,
    override val avatar: String?,
    override val `class`: Int
): ContentUser(firstname = firstname, password = password, avatar = avatar, `class` = `class`) {
    fun convertToSafe(): SafeUser {
        return SafeUser(id, firstname, avatar, `class`)
    }
}

open class ContentUser(
    open val firstname: String,
    open val password: String,
    open val avatar: String?,
    open val `class`: Int
){
    fun convertToUser(id: Int): User {
        return User(id, firstname, password, avatar, `class`)
    }
}

@Serializable
data class SafeUser(
    val id: Int,
    val firstname: String,
    val avatar: String?,
    val `class`: Int
)

object Users: Table(){
    val id = integer("id").autoIncrement()
    val firstname = varchar("firstname", 50)
    val avatar = varchar("avatar", 250).nullable()
    val `class` = integer("class")
    val password = varchar("password", 50)

    override val primaryKey = PrimaryKey(id)
}

fun Collection<User>.convertToSafe(): List<SafeUser>{
    return this.map { it.convertToSafe() }
}