package com.example.database.Token

import com.example.database.DAOTable
import com.example.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOToken: DAOTable<Token> {
    override fun resultRowToModel(row: ResultRow) = Token (
        id = row[Tokens.id],
        idUser = row[Tokens.idUser],
        token = row[Tokens.token]
    )

    override suspend fun selectAll(): List<Token> {
        return pushQuery{
            Tokens.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): Token? {
        return pushQuery {
            Tokens.select(where).map(::resultRowToModel).singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<Token> {
        return pushQuery {
            Tokens.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Tokens.deleteWhere(op=where) > 0
        }
    }

    override suspend fun insert(model: Token): Token? {
        return pushQuery {
            Tokens.insert {
                it[idUser] = model.idUser
                it[token] = model.token
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: Token, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Tokens.update(where){
                it[token] = model.token
                it[idUser] = model.idUser
            } > 0
        }
    }

    suspend fun editOnlyToken(token: String, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean{
        return pushQuery {
            Tokens.update(where){
                it[Tokens.token] = token
            } > 0
        }
    }

    suspend fun checkTokenUser(token: String, idUser: Int): Boolean{
        return selectSingle{
            (Tokens.idUser eq idUser) and (Tokens.token eq token)
        } != null
    }
}