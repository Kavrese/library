package com.example.database.Token

import org.jetbrains.exposed.sql.Table

data class Token(
    val id: Int,
    val idUser: Int,
    val token: String,
){
    fun convertToSafe(): SafeToken{
        return SafeToken(
            token=token
        )
    }
}

@kotlinx.serialization.Serializable
data class SafeToken(
    val token: String
)

object Tokens: Table("users_token"){
    val id = integer("id").autoIncrement()
    val token = varchar("token", 100)
    val idUser = integer("id_user")

    override val primaryKey = PrimaryKey(id)
}


