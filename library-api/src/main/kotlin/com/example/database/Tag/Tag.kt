package com.example.database.Tag

import org.jetbrains.exposed.sql.Table

@kotlinx.serialization.Serializable
class Tag {
    val id: Int
    val name: String
    val isTop: Boolean
    val isCategory: Boolean

    constructor(id: Int, name: String, isTop: Boolean, isCategory: Boolean){
        this.id = id
        this.name = name
        this.isTop = isTop
        this.isCategory = isCategory
    }
}

@kotlinx.serialization.Serializable
class BookTag {
    val id: Int
    val idBook: Int
    val idTag: Int

    constructor(id: Int, idBook: Int, idTag: Int){
        this.id = id
        this.idBook = idBook
        this.idTag = idTag
    }
}

object BooksTags: Table("books_tags"){
    val id = integer("id").autoIncrement()
    val idBook = integer("id_book")
    val idTag = integer("id_tag")

    override val primaryKey = PrimaryKey(id)
}

object Tags: Table(){
    val id = integer("id").autoIncrement()
    val name = varchar("name", 200)
    val isTop = bool("is_top")
    val isCategory = bool("is_category")

    override val primaryKey = PrimaryKey(id)
}