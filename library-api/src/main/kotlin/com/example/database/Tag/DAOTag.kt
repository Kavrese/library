package com.example.database.Tag

import com.example.database.DAOTable
import com.example.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOTag: DAOTable<Tag> {
    override fun resultRowToModel(row: ResultRow): Tag {
        return Tag(
            id = row[Tags.id],
            name = row[Tags.name],
            isTop = row[Tags.isTop],
            isCategory = row[Tags.isCategory]
        )
    }

    override suspend fun selectAll(): List<Tag> {
        return pushQuery{
            Tags.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): Tag? {
        return pushQuery {
            Tags.select(where).map(::resultRowToModel).singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<Tag> {
        return pushQuery {
            Tags.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Tags.deleteWhere(op = where) > 0
        }
    }

    override suspend fun insert(model: Tag): Tag? {
        return pushQuery {
            Tags.insert {
                it[Tags.name] = model.name
                it[Tags.isTop] = model.isTop
                it[Tags.isCategory] = model.isCategory
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: Tag, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Tags.update(where) {
                it[Tags.name] = model.name
                it[Tags.isTop] = model.isTop
                it[Tags.isCategory] = model.isCategory
            } > 0
        }
    }

    suspend fun checkExistTag(idTag: Int): Boolean{
        return selectSingle {
            Tags.id eq idTag
        } != null
    }

    suspend fun checkExistTag(tagTitle: String): Boolean{
        return selectSingle {
            Tags.name eq tagTitle
        } != null
    }
}