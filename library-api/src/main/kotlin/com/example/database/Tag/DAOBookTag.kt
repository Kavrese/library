package com.example.database.Tag

import com.example.database.DAOTable
import com.example.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOBookTag: DAOTable<BookTag> {
    override fun resultRowToModel(row: ResultRow): BookTag {
        return BookTag(
            id = row[BooksTags.id],
            idBook = row[BooksTags.idBook],
            idTag = row[BooksTags.idTag]
        )
    }

    override suspend fun selectAll(): List<BookTag> {
        return pushQuery{
            BooksTags.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): BookTag? {
        return pushQuery {
            BooksTags.select(where).map(::resultRowToModel).singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<BookTag> {
        return pushQuery {
            BooksTags.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            BooksTags.deleteWhere(op = where) > 0
        }
    }

    override suspend fun insert(model: BookTag): BookTag? {
        return pushQuery {
            BooksTags.insert {
                it[BooksTags.idTag] = model.idTag
                it[BooksTags.idBook] = model.idBook
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun edit(model: BookTag, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            BooksTags.update(where) {
                it[BooksTags.idTag] = model.idTag
                it[BooksTags.idBook] = model.idBook
            } > 0
        }
    }

    suspend fun checkExistBookTag(idTag: Int): Boolean{
        return selectSingle {
            BooksTags.id eq idTag
        } != null
    }
}