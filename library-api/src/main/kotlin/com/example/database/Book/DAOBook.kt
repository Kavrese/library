package com.example.database.Book

import com.example.database.DAOTable
import com.example.database.DatabaseFactory.pushQuery
import org.jetbrains.exposed.sql.*

class DAOBook: DAOTable<Book> {
    override fun resultRowToModel(row: ResultRow): Book {
        return Book(
            id=row[Books.id],
            title=row[Books.title],
            desc=row[Books.desc],
            cover=row[Books.cover]
        )
    }

    override suspend fun selectAll(): List<Book> {
        return pushQuery{
            Books.selectAll().map(::resultRowToModel)
        }
    }

    override suspend fun selectSingle(where: SqlExpressionBuilder.() -> Op<Boolean>): Book? {
        return pushQuery {
            Books.select(where).map(::resultRowToModel).singleOrNull()
        }
    }

    override suspend fun selectMany(where: SqlExpressionBuilder.() -> Op<Boolean>): List<Book> {
        return pushQuery {
            Books.select(where).map(::resultRowToModel)
        }
    }

    override suspend fun edit(model: Book, where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Books.update(where) {
                it[Books.title] = model.title
                it[Books.cover] = model.cover
                it[Books.desc] = model.desc
            } > 0
        }
    }

    override suspend fun insert(model: Book): Book? {
        return pushQuery {
            Books.insert {
                it[Books.title] = model.title
                it[Books.cover] = model.cover
                it[Books.desc] = model.desc
            }.resultedValues?.singleOrNull()?.let(::resultRowToModel)
        }
    }

    override suspend fun delete(where: SqlExpressionBuilder.() -> Op<Boolean>): Boolean {
        return pushQuery {
            Books.deleteWhere(op = where) > 0
        }
    }

    suspend fun checkExistBook(idBook: Int): Boolean{
        return selectSingle {
            Books.id eq idBook
        } != null
    }
}