package com.example.database.Book

import org.jetbrains.exposed.sql.Table

@kotlinx.serialization.Serializable
class Book {
    val id: Int
    val title: String
    val cover: String
    val desc: String

    constructor(id: Int, title:String, cover: String, desc: String){
        this.id = id
        this.title = title
        this.cover = cover
        this.desc = desc
    }
}

object Books: Table(){
    val id = integer("id").autoIncrement()
    val title = varchar("title", 250)
    val cover = varchar("cover", 500)
    val desc = varchar("desc", 5000)

    override val primaryKey = PrimaryKey(id)
}