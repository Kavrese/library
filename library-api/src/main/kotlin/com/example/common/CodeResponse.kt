package com.example.common

import com.example.common.models.ModelAnswer
import io.ktor.http.*

/*
    Custom code errors:
        10xx - Общие ошибки
            1000 - NEW_TOKEN_NOT_CREATED / Новый токен не был присвоен пользователю
        11xx - Ошибки авторизации:
            1100 - SCHOOL_PORTAL_NOT_AVAILABLE / Школьный портал не работает
        12xx - Ошибки регистрации:
            1200 - USER_NOT_CREATED / Пользователь не был создан
*/

@kotlinx.serialization.Serializable
class CodeResponse {
    val code: Int
    val description: String

    constructor(code: Int): this(code, "")
    constructor(httpStatusCode: HttpStatusCode, description: String): this(httpStatusCode.value, description)
    constructor(httpStatusCode: HttpStatusCode, codeTitle: CodeTitle): this(httpStatusCode.value, codeTitle)
    constructor(code: Int, codeTitle: CodeTitle): this(code, codeTitle.toString())
    constructor(httpStatusCode: HttpStatusCode): this(httpStatusCode.value, httpStatusCode.description)
    constructor(code: Int, description: String){
        this.description = description
        this.code = code
    }

    fun convertToHttpsStatusCode(): HttpStatusCode{
        return HttpStatusCode(code, description)
    }

    fun <T> toModelAnswer(body: T?=null): ModelAnswer<T> {
        return ModelAnswer(this, body)
    }

    companion object {
        fun HttpStatusCode.toCodeResponse(): CodeResponse {
            return CodeResponse(this)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other is HttpStatusCode) return code == other.value
        return super.equals(other)
    }
    /*
    Было созданно автоматически IDEей
     */
    override fun hashCode(): Int {
        var result = code
        result = 31 * result + description.hashCode()
        return result
    }
}

enum class CodeTitle(private val title: String){
    USER_NOT_FOUND("User not found"),
    BOOK_NOT_FOUND("Book not found"),
    TAG_NOT_FOUND("Tag not found"),
    FILE_NOT_FOUND("File not found"),
    AUTHOR_NOT_FOUND("Author not found"),
    PASSWORD_8("Password not correct (min 8 length)"),
    NAME_ALREADY_EXIST("This firstname already exist"),
    USER_NOT_CREATED("User not created"),
    NEW_TOKEN_NOT_INSERT("New token not created"),
    CONTENT_BOOK_NOT_FOUND("Content book not found"),
    MASTER_PASSWORD_NOT_CORRECT("Master password not correct"),
    SCHOOL_PORTAL_NOT_AVAILABLE("School portal not available");

    override fun toString(): String {
        return title
    }
}

enum class CallbackCodeResponse(val codeResponse: CodeResponse){
    NEW_TOKEN_NOT_INSERT(CodeResponse(1000, CodeTitle.NEW_TOKEN_NOT_INSERT)),

    SCHOOL_PORTAL_NOT_AVAILABLE(CodeResponse(1100, CodeTitle.SCHOOL_PORTAL_NOT_AVAILABLE)),

    USER_NOT_CREATED(CodeResponse(1200, CodeTitle.USER_NOT_CREATED)),

    USER_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.USER_NOT_FOUND)),
    BOOK_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.BOOK_NOT_FOUND)),
    TAG_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.TAG_NOT_FOUND)),
    FILE_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.FILE_NOT_FOUND)),
    AUTHOR_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.AUTHOR_NOT_FOUND)),
    CONTENT_BOOK_NOT_FOUND(CodeResponse(HttpStatusCode.NotFound, CodeTitle.CONTENT_BOOK_NOT_FOUND)),

    NAME_ALREADY_EXIST(CodeResponse(HttpStatusCode.NotAcceptable, CodeTitle.NAME_ALREADY_EXIST)),

    MASTER_PASSWORD_NOT_CORRECT(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.MASTER_PASSWORD_NOT_CORRECT)),

    PASSWORD_NOT_CORRECT_8(CodeResponse(HttpStatusCode.BadRequest, CodeTitle.PASSWORD_8)),

}
