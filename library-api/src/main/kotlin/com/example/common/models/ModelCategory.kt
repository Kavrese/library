package com.example.common.models

import com.example.features.books.FullBook

@kotlinx.serialization.Serializable
data class ModelCategory<T>(
    val title: String,
    val items: List<T>
)
