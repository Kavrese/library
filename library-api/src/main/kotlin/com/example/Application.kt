package com.example

import com.example.common.CodeResponse
import com.example.common.models.ModelAnswer
import com.example.database.BookFile.BookFile
import com.example.database.DatabaseFactory
import com.example.features.auth.configureAuthRouting
import com.example.features.author.configureAuthorRouting
import com.example.features.books.configureBooksRouting
import com.example.features.contentbook.configureContentBookRouting
import com.example.features.files.configureFilesRouting
import com.example.features.foradmin.configureInsertDataRouting
import com.example.features.tags.configureTagsRouting
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.example.plugins.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.callloging.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.doublereceive.*
import io.ktor.server.response.*
import io.ktor.server.util.*
import org.slf4j.event.Level
import java.io.File

fun main() {
    embeddedServer(Netty, port = 8080, host = "192.168.1.66") {
        install(ContentNegotiation) {
            json()
        }
        install(DoubleReceive)
        install(CallLogging){
            level = Level.INFO
            format {
                it.url()
            }
        }
        DatabaseFactory.initDataBase()
        configureAuthRouting()
        configureBooksRouting()
        configureAuthorRouting()
        configureTagsRouting()
        configureContentBookRouting()
        configureFilesRouting()
        configureTestRouting()
        configureInsertDataRouting()
    }.start(wait = true)
}

suspend fun ApplicationCall.respondExceptionError(e: Exception){
    respondAnswer(ModelAnswer<String>(CodeResponse(400, e.message ?: e.localizedMessage ?: e.toString())))
}

suspend fun ApplicationCall.respondNullExceptionErrorQueryParameters(e: NullPointerException, parameters: List<String>){
    val message = "$e - check this field: $parameters"
    respondAnswer(ModelAnswer<String>(CodeResponse(HttpStatusCode.BadRequest, message)))
}

suspend inline fun <reified T> ApplicationCall.respondAnswer(model: ModelAnswer<T>){
    respond(HttpStatusCode.OK, model)
}

suspend inline fun ApplicationCall.respondFileAnswer(answer: ModelAnswer<BookFile>){
    if (answer.body != null)
        respondFile(answer.body.filePath, answer.body.filename)
    else
        respondAnswer(answer)
}

suspend inline fun ApplicationCall.respondFile(pathFile: String, filename: String){
    response.header(
        HttpHeaders.ContentDisposition,
        ContentDisposition.Attachment.withParameter(
            ContentDisposition.Parameters.FileName, filename
        ).toString()
    )
    respondFile(File(pathFile))
}

