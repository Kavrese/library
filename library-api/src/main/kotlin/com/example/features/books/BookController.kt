package com.example.features.books

import com.example.common.CallbackCodeResponse
import com.example.common.models.ModelAnswer
import com.example.database.Book.Book
import com.example.database.Book.Books
import com.example.database.Book.DAOBook
import org.jetbrains.exposed.sql.upperCase

class BookController {
    private val daoBook = DAOBook()

    suspend fun getBook(idBook: Int): ModelAnswer<Book>{
        val book = daoBook.selectSingle {
            Books.id eq idBook
        } ?: return ModelAnswer(CallbackCodeResponse.BOOK_NOT_FOUND)

        return ModelAnswer(book)
    }

    suspend fun searchBooksTitle(title: String): List<Book>{
        val books = daoBook.selectMany {
            Books.title.upperCase() like "%${title.uppercase()}%"
        }
        return books
    }

    suspend fun getAllBook(): List<Book>{
        return daoBook.selectAll()
    }

    suspend fun checkExistBook(id: Int): Boolean{
        return daoBook.checkExistBook(id)
    }
}