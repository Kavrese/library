package com.example.features.books

import com.example.common.CallbackCodeResponse
import com.example.common.models.ModelAnswer
import com.example.database.Author.Author
import com.example.database.Book.Book
import com.example.database.Tag.Tag
import com.example.features.author.AuthorController
import com.example.features.author.BookAuthorController
import com.example.features.tags.BookTagController
import com.example.features.tags.TagController

class DataControllerMerger {
    val bookController = BookController()
    val authorController = AuthorController()
    val bookAuthorController = BookAuthorController()
    val tagController = TagController()
    val bookTagController = BookTagController()

    suspend fun getFullBook(idBook: Int): ModelAnswer<FullBook>{
        val general = bookController.getBook(idBook).body ?: return ModelAnswer(CallbackCodeResponse.BOOK_NOT_FOUND)
        val tags = bookTagController.getTagsBook(idBook).mapNotNull {
            tagController.getTag(it.idTag).body
        }
        val authors = bookAuthorController.getAuthorsBook(idBook).mapNotNull {
            authorController.getAuthor(it.idAuthor).body
        }
        return ModelAnswer(
            FullBook(
                general = general,
                tags = tags,
                authors = authors
            )
        )
    }

    suspend fun getFullBooksTag(idTag: Int): ModelAnswer<List<FullBook>>{
        return ModelAnswer(bookTagController.getBooksTag(idTag).mapNotNull {
            getFullBook(it.idBook).body
        })
    }

    suspend fun getBooksTag(idTag: Int): ModelAnswer<List<Book>>{
        return ModelAnswer(bookTagController.getBooksTag(idTag).mapNotNull {
            bookController.getBook(it.idBook).body
        })
    }

    suspend fun getAllFullBooks(): ModelAnswer<List<FullBook>>{
        return  ModelAnswer(bookController.getAllBook().mapNotNull {
            getFullBook(it.id).body
        })
    }

    suspend fun getTopPopularTags(countTags: Int): List<Tag>{
        val allTags = tagController.getAllTags().body!!
        val allTagsIds = allTags.map{it.id}
        val mapIdTagCount = bookTagController.getCountBookTags(allTagsIds)
        allTags.sortedByDescending {
            mapIdTagCount[it.id]
        }
        return allTags.subList(0, countTags)
    }
}

@kotlinx.serialization.Serializable
data class FullBook(
    val general: Book,
    val authors: List<Author>,
    val tags: List<Tag>
)