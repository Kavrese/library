package com.example.features.books

import com.example.common.models.ModelAnswer
import com.example.common.models.ModelCategory
import com.example.respondAnswer
import com.example.respondExceptionError
import com.example.respondNullExceptionErrorQueryParameters
import io.ktor.server.application.*
import io.ktor.server.routing.*

fun Application.configureBooksRouting() {
    val dataControllerMerger = DataControllerMerger()

    routing {
        get("/book"){
            try{
                val id = call.request.queryParameters["id_book"]!!.toInt()
                val resultBook = dataControllerMerger.getFullBook(id)
                call.respondAnswer(resultBook)
            } catch (e: NullPointerException) {
                call.respondNullExceptionErrorQueryParameters(e, listOf("id_book"))
            }catch(e: Exception){
                call.respondExceptionError(e)
            }
        }

        get("/books_title"){
            try {
                val queryTitle = call.request.queryParameters["title"]!!
                val books = dataControllerMerger.bookController.searchBooksTitle(queryTitle).map {
                    dataControllerMerger.getFullBook(it.id).body
                }
                val result = ModelAnswer(books)
                call.respondAnswer(result)
            } catch (e: NullPointerException) {
                call.respondNullExceptionErrorQueryParameters(e, listOf("title"))
            }catch(e: Exception){
                call.respondExceptionError(e)
            }
        }

        get("/books_tag"){
            try {
                val idTag = call.request.queryParameters["id_tag"]!!.toInt()
                val books = dataControllerMerger.getFullBooksTag(idTag)
                call.respondAnswer(books)
            } catch (e: NullPointerException) {
                call.respondNullExceptionErrorQueryParameters(e, listOf("id_tag"))
            }catch(e: Exception){
                call.respondExceptionError(e)
            }
        }

        get("/books_popular_categories"){
            try {
                val count = call.request.queryParameters["count"]?.toInt() ?: 5
                val popularTags = dataControllerMerger.getTopPopularTags(count)
                val resultList = popularTags.map {
                    ModelCategory(it.name, dataControllerMerger.getBooksTag(it.id).body ?: listOf())
                }
                val answer = ModelAnswer(resultList)
                call.respondAnswer(answer)
            } catch (e: NullPointerException) {
                call.respondNullExceptionErrorQueryParameters(e, listOf("count"))
            } catch(e: Exception){
                call.respondExceptionError(e)
            }
        }

        get("/books_categories"){
            try{
                val tagsCategories = dataControllerMerger.tagController.getTagCategories()
                val resultList = tagsCategories.map {
                    ModelCategory(it.name, dataControllerMerger.getBooksTag(it.id).body ?: listOf())
                }
                val answer = ModelAnswer(resultList)
                call.respondAnswer(answer)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get("/books"){
            try{
                val books = dataControllerMerger.bookController.getAllBook()
                call.respondAnswer(ModelAnswer(books))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get("/exist_book"){
            try{
                val id = call.request.queryParameters["id_book"]!!.toInt()
                val existBook = dataControllerMerger.bookController.checkExistBook(id)
                call.respondAnswer(ModelAnswer(existBook))
            } catch (e: NullPointerException) {
                call.respondNullExceptionErrorQueryParameters(e, listOf("id"))
            }catch(e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}