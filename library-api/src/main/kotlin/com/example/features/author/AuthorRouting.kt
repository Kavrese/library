package com.example.features.author

import com.example.common.models.ModelAnswer
import com.example.respondAnswer
import com.example.respondExceptionError
import com.example.respondNullExceptionErrorQueryParameters
import io.ktor.server.application.*
import io.ktor.server.routing.*

fun Application.configureAuthorRouting() {
    val authorController = AuthorController()

    routing {
        get("/author"){
            val parameters = call.request.queryParameters
            try{
                try {
                    val id = parameters["id"]!!.toInt()
                    call.respondAnswer(authorController.getAuthor(id))
                }catch (e: Exception){
                    val name = parameters["name"]!!.toString()
                    val authors = authorController.getAuthors(name)
                    call.respondAnswer(ModelAnswer(authors))
                }
            }catch (e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("id or name"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}