package com.example.features.author

import com.example.database.Author.AuthorBook
import com.example.database.Author.AuthorsBook
import com.example.database.Author.DAOAuthorBook

class BookAuthorController {
    private val daoAuthorBook = DAOAuthorBook()

    suspend fun getAuthorsBook(idBook: Int): List<AuthorBook>{
        val authors = daoAuthorBook.selectMany {
            AuthorsBook.idBook eq idBook
        }

        return authors
    }

    suspend fun getBooksAuthor(idAuthor: Int): List<AuthorBook>{
        val books = daoAuthorBook.selectMany {
            AuthorsBook.idAuthor eq idAuthor
        }

        return books
    }
}