package com.example.features.author

import com.example.common.CallbackCodeResponse
import com.example.common.models.ModelAnswer
import com.example.database.Author.Author
import com.example.database.Author.Authors
import com.example.database.Author.DAOAuthor

class AuthorController {
    private val daoAuthor = DAOAuthor()

    suspend fun getAuthor(id: Int): ModelAnswer<Author>{
        val author = daoAuthor.selectSingle {
            Authors.id eq id
        } ?: return ModelAnswer(CallbackCodeResponse.AUTHOR_NOT_FOUND)

        return ModelAnswer(author)
    }

    suspend fun getAuthors(name: String): List<Author>{
        val allData = mutableListOf<Author>()
        allData.addAll(daoAuthor.selectMany {
            Authors.name like "%$name%"
        })
        return allData.toSet().toList()
    }
}