package com.example.common

import com.example.database.Author.*
import com.example.database.Book.Book
import com.example.database.Book.DAOBook
import com.example.database.BookFile.BookFile
import com.example.database.BookFile.DAOBookFile
import com.example.database.Tag.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class InsertDataController {

    private val daoBook = DAOBook()
    private val daoTag = DAOTag()
    private val daoBookTag = DAOBookTag()
    private val daoAuthor = DAOAuthor()
    private val daoAuthorBook = DAOAuthorBook()
    private val daoBookFile = DAOBookFile()

    suspend fun insertBookModel(data: List<ModelBook>){
        for (modelBook in data){
            val idBook = daoBook.insert(modelBook.convertToBook())!!.id
            modelBook.tags.forEach {
                val idTag = if (!daoTag.checkExistTag(it)) {
                    daoTag.insert(Tag(-1, it, false, false))!!.id
                }else{
                    daoTag.selectSingle {
                        Tags.name eq it
                    }!!.id
                }
                daoBookTag.insert(BookTag(-1, idBook, idTag))
            }
            modelBook.authors.forEach {
                val idAuthor = if (!daoAuthor.checkExistAuthor(it)){
                    daoAuthor.insert(Author(-1, it))!!.id
                }else{
                    daoAuthor.selectSingle { Authors.name eq it }!!.id
                }
                daoAuthorBook.insert(
                    AuthorBook(-1, idAuthor, idBook)
                )
            }

            if (!daoBookFile.checkExistBookFile(modelBook.file_path, idBook))
                daoBookFile.insert(BookFile(-1, modelBook.file_path, idBook, getFilenameFromPath(modelBook.file_path)))
        }
    }

    suspend fun insertBookJson(json: String){
        val data = convertJsonToModel<List<ModelBook>>(json)
        insertBookModel(data)
    }

    private fun getFilenameFromPath(path: String): String{
        return path.reversed().substringAfter(".").substringBefore("\\")
    }

    private inline fun <reified T : Any> convertJsonToModel(json: String): T{
        return Json.decodeFromString(json)
    }
}

@kotlinx.serialization.Serializable
data class ModelBook(
    val title: String,
    val authors: List<String>,
    val desc: String,
    val tags: List<String>,
    val cover: String,
    val file_path: String
){
    fun convertToBook(): Book = Book(-1, title, cover, desc)
}