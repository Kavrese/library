package com.example.features.foradmin

import com.example.common.CallbackCodeResponse
import com.example.common.InsertDataController
import com.example.common.ModelBook
import com.example.common.models.ModelAnswer
import com.example.respondAnswer
import com.example.respondExceptionError
import com.example.respondNullExceptionErrorQueryParameters
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureInsertDataRouting(){

    val insertDataController = InsertDataController()

    routing {
        post("/insert_books"){
            try {
                val body = call.receive<List<ModelBook>>()
                val masterPassword = call.parameters["master_password"]
                if (masterPassword == "ktytreag45745avdsf1425") {
                    insertDataController.insertBookModel(body)
                    call.respond(true)
                }else{
                    call.respondAnswer(ModelAnswer<String>(CallbackCodeResponse.MASTER_PASSWORD_NOT_CORRECT))
                }
            }catch (e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("master_password"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}