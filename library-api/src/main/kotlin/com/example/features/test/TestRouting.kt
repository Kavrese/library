package com.example.plugins

import io.ktor.server.routing.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*

fun Application.configureTestRouting() {
    routing {
        get("/test") {
            val params = call.request.queryParameters.entries()
            call.respond(params)
        }
        post("/test") {
            val body = call.receive<Map<String, String>>()
            call.respond(body)
        }
        get("/hello") {
            call.respondText { "Hello World !" }
        }
    }
}
