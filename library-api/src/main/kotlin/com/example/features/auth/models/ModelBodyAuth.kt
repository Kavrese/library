package com.example.features.auth.models
@kotlinx.serialization.Serializable
data class ModelBodyAuth(
    val login: String,
    val password: String,
)
@kotlinx.serialization.Serializable
data class ModelBodyReg(
    val login: String,
    val password: String,
    val `class`: Int
){
    fun toModelBodyAuth(): ModelBodyAuth{
        return ModelBodyAuth(login, password)
    }
}
