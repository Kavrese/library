package com.example.features.auth

import com.example.features.auth.models.ModelBodyAuth
import com.example.features.auth.models.ModelBodyReg
import com.example.respondAnswer
import com.example.respondExceptionError
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureAuthRouting() {
    val authController = AuthController()

    routing {
        post("/login"){
            try{
                val modelBodyAuth = call.receive<ModelBodyAuth>()
                val result = authController.login(modelBodyAuth)
                call.respondAnswer(result)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
        post("/login_school_api"){
            val modelBodyAuth = call.receive<ModelBodyAuth>()
            val result = authController.loginSchoolAPI(modelBodyAuth)
        }
        post("/reg"){
            try {
                val modelBodyReg = call.receive<ModelBodyReg>()
                val result = authController.reg(modelBodyReg)
                call.respondAnswer(result)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}

