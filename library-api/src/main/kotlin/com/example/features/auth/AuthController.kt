package com.example.features.auth

import com.example.common.CallbackCodeResponse
import com.example.common.CodeResponse
import com.example.common.models.ModelAnswer
import com.example.database.Token.SafeToken
import com.example.database.User.DAOUser
import com.example.database.User.User
import com.example.database.User.Users
import com.example.features.auth.models.ModelBodyAuth
import com.example.features.auth.models.ModelBodyReg
import com.example.features.token.TokenController
import org.jetbrains.exposed.sql.and
import java.util.UUID

class AuthController {

    private val daoUsers = DAOUser()
    private val tokenController = TokenController()

    suspend fun login(modelBodyAuth: ModelBodyAuth): ModelAnswer<SafeToken> {
        val user = daoUsers.selectSingle {
            (Users.password eq modelBodyAuth.password) and (Users.firstname eq modelBodyAuth.login)
        }
        if (user != null){
            return generateNewToken(user.id)
        }
        return ModelAnswer(CallbackCodeResponse.USER_NOT_FOUND)
    }

    suspend fun loginSchoolAPI(modelBodyAuth: ModelBodyAuth): CodeResponse {
        val token = UUID.randomUUID().toString()
        return CallbackCodeResponse.SCHOOL_PORTAL_NOT_AVAILABLE.codeResponse
    }

    private suspend fun generateNewToken(idUser: Int): ModelAnswer<SafeToken>{
        val answer = tokenController.generateEditNewTokenForUser(idUser)
        if (answer != null)
            return ModelAnswer(SafeToken(answer))
        return ModelAnswer(CallbackCodeResponse.USER_NOT_FOUND)
    }

    suspend fun reg(modelBodyReg: ModelBodyReg): ModelAnswer<SafeToken> {
        if (modelBodyReg.password.length < 8)
            return ModelAnswer(CallbackCodeResponse.PASSWORD_NOT_CORRECT_8)
        if (daoUsers.checkExistUserName(modelBodyReg.login)){
            return ModelAnswer(CallbackCodeResponse.NAME_ALREADY_EXIST)
        }
        // Нормальный id user автоматически получит при добавлении в базу
        val newUser = daoUsers.insert(
            User(-1, modelBodyReg.login, modelBodyReg.password, null, modelBodyReg.`class`)
        ) ?: return ModelAnswer(CallbackCodeResponse.USER_NOT_CREATED)

        return tokenController.generateCreateNewTokenForUser(newUser.id)
    }
}