package com.example.features.token

import com.example.common.models.ModelAnswer
import com.example.database.Token.DAOToken
import com.example.database.Token.SafeToken
import com.example.database.Token.Tokens
import com.example.common.CallbackCodeResponse
import com.example.database.Token.Token
import io.ktor.http.*
import java.util.*

class TokenController {
    private val daoToken = DAOToken()

    suspend fun generateCreateNewTokenForUser(idUser: Int): ModelAnswer<SafeToken>{
        val newToken = UUID.randomUUID().toString()
        val result = daoToken.insert(Token(id = -1, idUser = idUser, token = newToken))
        return if (result != null){
            ModelAnswer(result.convertToSafe())
        }else{
            ModelAnswer(CallbackCodeResponse.NEW_TOKEN_NOT_INSERT)
        }
    }

    suspend fun generateEditNewTokenForUser(idUser: Int): String?{
        val newToken = UUID.randomUUID().toString()
        val result = daoToken.editOnlyToken(newToken){
            Tokens.idUser eq idUser
        }
        return if(result) {
            newToken
        }else{
            null
        }
    }

    suspend fun getTokenUser(idUser: Int): ModelAnswer<SafeToken>{
        val modelToken = daoToken.selectSingle {
            Tokens.idUser eq idUser
        } ?: return ModelAnswer(CallbackCodeResponse.USER_NOT_FOUND)

        return ModelAnswer(HttpStatusCode.OK, modelToken.convertToSafe())
    }
}