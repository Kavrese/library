package com.example.features.files

import com.example.respondExceptionError
import com.example.respondFileAnswer
import com.example.respondNullExceptionErrorQueryParameters
import io.ktor.server.application.*
import io.ktor.server.routing.*

fun Application.configureFilesRouting() {
    val fileController = FileController()

    routing {
        get("/book_file"){
            try {
                val idBook = call.request.queryParameters["id_book"]!!.toInt()
                val file = fileController.getBookFile(idBook)
                call.respondFileAnswer(file)
            }catch (e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("id_book"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}