package com.example.features.files

import com.example.common.CallbackCodeResponse
import com.example.common.models.ModelAnswer
import com.example.database.BookFile.BookFile
import com.example.database.BookFile.BookFiles
import com.example.database.BookFile.DAOBookFile

class FileController {
    private val daoBookFile = DAOBookFile()

    suspend fun getBookFile(idBook: Int): ModelAnswer<BookFile>{
        val file = daoBookFile.selectSingle {
            BookFiles.idBook eq idBook
        } ?: return ModelAnswer(CallbackCodeResponse.FILE_NOT_FOUND)

        return ModelAnswer(file)
    }
}