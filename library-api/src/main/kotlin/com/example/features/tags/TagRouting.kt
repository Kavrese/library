package com.example.features.tags

import com.example.respondAnswer
import com.example.respondExceptionError
import io.ktor.server.application.*
import io.ktor.server.routing.*

fun Application.configureTagsRouting() {

    val tagController = TagController()

    routing {
        get("/top_tags"){
            try {
                val tags = tagController.getTopTags()
                call.respondAnswer(tags)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get("/not_top_tags"){
            try {
                val notTags = tagController.getNotTopTags()
                call.respondAnswer(notTags)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }

        get("/tags"){
            try {
                val tags = tagController.getAllTags()
                call.respondAnswer(tags)
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}