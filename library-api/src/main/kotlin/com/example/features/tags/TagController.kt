package com.example.features.tags

import com.example.common.CallbackCodeResponse
import com.example.common.models.ModelAnswer
import com.example.database.Tag.DAOTag
import com.example.database.Tag.Tag
import com.example.database.Tag.Tags

class TagController {
    private val daoTag = DAOTag()

    suspend fun getTag(idTag: Int): ModelAnswer<Tag>{
        val tag = daoTag.selectSingle {
            Tags.id eq idTag
        } ?: return ModelAnswer(CallbackCodeResponse.TAG_NOT_FOUND)

        return ModelAnswer(tag)
    }

    suspend fun getTopTags(): ModelAnswer<List<Tag>>{
        return ModelAnswer(daoTag.selectMany {
            Tags.isTop eq true
        })
    }

    suspend fun getNotTopTags(): ModelAnswer<List<Tag>>{
        return ModelAnswer(daoTag.selectMany {
            Tags.isTop eq false
        })
    }

    suspend fun getTagCategories(): List<Tag>{
        return daoTag.selectMany {
            Tags.isCategory eq true
        }
    }

    suspend fun getAllTags(): ModelAnswer<List<Tag>>{
        return ModelAnswer(daoTag.selectAll())
    }
}