package com.example.features.tags

import com.example.database.Tag.BookTag
import com.example.database.Tag.BooksTags
import com.example.database.Tag.DAOBookTag

class BookTagController {
    private val daoBookTag = DAOBookTag()

    suspend fun getTagsBook(idBook: Int): List<BookTag>{
        val idsTags = daoBookTag.selectMany {
            BooksTags.idBook eq idBook
        }

        return idsTags
    }

    suspend fun getBooksTag(idTag: Int): List<BookTag>{
        val idsBooks = daoBookTag.selectMany {
            BooksTags.idTag eq idTag
        }

        return idsBooks
    }

    suspend fun getCountBookTags(idsTags: List<Int>): Map<Int, Int>{
        val data = mutableMapOf<Int, Int>()
        idsTags.forEach {
            data[it] = getCountBookTag(it)
        }
        return data
    }

    suspend fun getCountBookTag(idTag: Int): Int{
        return daoBookTag.selectMany {
            BooksTags.idTag eq idTag
        }.size
    }
}