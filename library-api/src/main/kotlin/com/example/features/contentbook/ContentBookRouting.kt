package com.example.features.contentbook

import com.example.respondAnswer
import com.example.respondExceptionError
import com.example.respondNullExceptionErrorQueryParameters
import io.ktor.server.routing.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*

fun Application.configureContentBookRouting() {

    val contentBookController = ContentBookController()

    routing {
        get("/content_book") {
            try {
                val id = call.request.queryParameters["id_book"]!!
                val result = contentBookController.getContentBook(id.toInt())
                call.respondAnswer(result)
            }catch (e: NullPointerException){
                call.respondNullExceptionErrorQueryParameters(e, listOf("id_book"))
            }catch (e: Exception){
                call.respondExceptionError(e)
            }
        }
    }
}
