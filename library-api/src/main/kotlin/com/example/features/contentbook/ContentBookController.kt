package com.example.features.contentbook

import com.example.common.models.ModelAnswer
import com.example.database.ContentBook.*
import com.example.database.ContentBook.DAOContentPartBook

class ContentBookController {
    private val daoContentPartBook = DAOContentPartBook()
    private val daoContentChapterBook = DAOContentChapterBook()

    suspend fun getContentBook(idBook: Int): ModelAnswer<FullContentBook>{
        val chapters = daoContentChapterBook.selectMany { ContentChapters.idBook eq idBook }
        val parts = daoContentPartBook.selectMany { ContentParts.idBook eq idBook }.map { itPart ->
            itPart.toPartBookChapters(
                chapters.filter { itChapter -> itChapter.idPart == itPart.id }
            )
        }.map {
            if (it.page == null && it.chapters.isNotEmpty()){
                it.page = it.chapters[0].page
            }
            it
        }.sortedBy {
            it.index
        }

        return ModelAnswer(FullContentBook(idBook, parts))
    }
}